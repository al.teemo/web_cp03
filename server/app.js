const express = require('express')
const app = express()
const port = 5000

app.get(`/`, (request, response) => {
    response.sendFile(__dirname + '/client/index.html');
})
// app.get(`/data/users.json`, (request, response) => {
//     console.log(`/data/users.json`);
//     response.sendFile(__dirname + '/data/users.json');
// })


app.use(express.static(__dirname+'/client'));
app.use(express.static(__dirname+'/data'));

// app.get(`/ans`, (request, response) => {
//     console.log('send file')
//     response.sendFile(__dirname + '/answer.json');
//     // console.log(response);
// })

app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }    
    console.log(`server is listening on ${port}`)
    // add('/main.css')
    // add('/jquery-3.4.1.js')
    // add('/test.js')
})

// function add(filename){
//     app.get(filename, (req, res) => {
//         console.log(__dirname+filename)
//         res.sendFile(__dirname+filename)
//     })
// }
