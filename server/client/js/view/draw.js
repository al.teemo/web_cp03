function Drow (){}

// === creation main page ===
Drow.crBody = function(username){
    // console.log('crBody')
    Drow.clearBody();
    $('body').prepend(
        $('<header>').append(
            $('<h1>',{text: 'kek'}),[
            $('<div>', {
                id: 'loginName',
                text: username || 'A1T'
            }),
            Drow.crInput('btnExit', 'button', 'exit', 'Logout')
        ])
    )    
// click logout
    $('input#btnExit').on('click', function(event){
        Page.getStart();
    });


    $('<main>').appendTo('body');
}

Drow.clearMain = function(){ $('main').empty();}
Drow.clearBody = function(){ $('body').empty();}
Drow.setHeadName = function(name){
    $('header h1:first').text(name);
}
Drow.crInput = function(id, type, clss, value){
    return $('<input>', {
        id: id || '',
        class: clss || 'loginInput',
        type: type || 'text',
        value: value || ''
    })
}
// ******* common create table elements *******
Drow.crCartTable = function(head, table, total){
    var tab = Drow.crTable('cartTable', head, table);
    $(tab).find('thead').after(
        $('<tfoot>',{
        }).append(
            Drow.crCell('th', 'Total:').attr('colspan', '3'),[
            Drow.crCell('th', total || '-', 'total'),
            ( head.length != 4
                ? Drow.crCell('th')
                : null
            )
        ])
    )
    return tab;
}
Drow.crTable = function(id, tableHead, tableList){
    return $('<table>', {
            id: id,
            bgcolor: '#333',
            border: 1,
            bordercolor: '#422',
            // align: 'center',
            rules: 'all',
            cellpadding: '5px',
            width: '100%'
        })
        .append(
            $('<col>', {
                // width: '65%'
            }),[
            $('<col>', {
                width: '40px',
                align: 'char',
                char: '.'//????????????????????
            }),
            $('<col>', {
                width: '20px'
            }),
            Drow.crtHead(tableHead),
            Drow.crtBody(tableList)
    ])
}
Drow.crtHead = function(headerList){
    return $('<thead>', {
        append: Drow.crRow('th', headerList)
    })
}
Drow.updBody = function(tab, tableList){
    tab.find('tbody').remove();
    tab.append(Drow.crtBody(tableList));
}
Drow.crtBody = function(tableList){
    return $('<tbody>').each(
        function (){
            for( var r of (tableList || []) )
                $(this).append(Drow.crRow('td', r));
        }
    )
}
Drow.crRow = function(tag, cellList){
    return $('<tr>').each(
        function (){
            for(var c of cellList || [])
                $(this).append(Drow.crCell(tag , c));
        }
    )
}
Drow.crCell = function(tag, text, name){
    return $("<"+(tag || 'td')+">", {
        name: name || 'cell',
        text: text || '',
        align: 'left'
    });
}
Drow.setUsername = function(name){
    $('#loginName').text(name || 'unknown');
}