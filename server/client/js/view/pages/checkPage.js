// =========== creation page of CHECK ===============
function CheckPage () {
    Page.call(this);
    this.myname = 'Check';

    this.render = function(){
        console.log('check render')
        if($('main').length == 0)
            Drow.crBody();
        else
            Drow.clearMain();
        
        Controller.getCheck(crCheck);
    }
    function crCheck(check){
        check = check.data;
        console.log(check)
        Drow.setHeadName('Check');
        $('main').append(
            $('<div>', {
                class: 'wrapper'
            }).append(
                $('<div>', {
                    id: 'checkList',
                    class: 'wrapper'
                }).append(
                    Drow.crCartTable(check.head, check.table, check.total)
                ),[
                Drow.crInput('exit', 'button', 'exit', 'Exit')
            ])
        )
        $('input#exit').on('click', function(event){
            Page.getStart();
        })
    }
}