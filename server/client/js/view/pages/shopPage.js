//TODO: выровнять последние столбцы
//TODO: добавить анимацию в шапке

// ========= creation page of Select product ==========
function ShopPage(){
    Page.call(this);
    var that = this;
    this.myname = 'shoppping';

    this.render = function(){
        console.log('shop render');
        if($('main').length == 0)
            Drow.crBody();

        Controller.crShop(that.crTables);
        Controller.getUsername(Drow.setUsername);
    }

    this.crTables = function(heads){
        // console.log('crTables')
        Drow.setHeadName('Product selection');
        $('main').append(
            $('<div>', {
                class: 'wrapper'
            }).append(
                Drow.crTable('prodSelTable', heads.prod),[
                $('<br>'),
                // Drow.crInput('showBuyerList', 'button', 'exit', 'Show basket'),
                $('<div>', {
                    id: 'cartList',
                    class: 'wrapper'
                }).append(
                    Drow.crCartTable(heads.cart),[
                    Drow.crInput('buy', 'button', 'exit', 'Buy selected')
                ])
            ])
        );
    // --- fill table of data ---
        that.updProd();
        that.updCart();
    // click 'buy'
        $('input#buy').on('click', function(event){
            Page.nextPage();
        })
    }


    this.updProd = function(){
        Controller.updTable(that.updProdTable);
    }
    this.updProdTable = function(prod){
        Drow.updBody( $('#prodSelTable'), prod );
    // --- add '+' to products ---
        $('#prodSelTable tbody tr').each(function(i, el){
            $(el).append(
                Drow.crCell().append(
                    Drow.crInput('addt'+i, 'text', 'addProd', '1').attr(
                        'size', '1'
                    ),[
                    Drow.crInput('addb'+i, 'button', 'addProd', '+')
                ])
            )
            // .attr('id', i);
        });
    // click table '+'
        $('table#prodSelTable input[type="button"]').each((i, el) => {
            $(el).on('click', event => {
                Controller.addItem(
                    i, $(el).siblings('#addt'+i).val(),
                    that.updCartTable // callback
                );
            })
        });
    }

    this.updCart = function(){
        Controller.updCart(that.updCartTable);
    }
    this.updCartTable = function(cart){
        var tab = $('#cartTable');
        // console.log('cart')
        if(cart != null){
            // console.log(cart.table);
            Drow.updBody(tab, cart.table);
            tab.find('th[name = "total"]').text(cart.total || '-');
        }

        tab.find('tbody tr').each(function(i, row){
            $(row).append(
                Drow.crCell().append(
                    Drow.crInput('dec', 'button', 'addProd', '-1'),[
                    Drow.crInput('rm', 'button', 'addProd', 'all')
                ]).attr('id', 'remove'+i)
            )
        })
    // click cart 'rm' / '-'
        $('table#cartTable td[id^="remove"]').each((i, rm) => {
            var params = {
                "id" : i,
                'remove' : ()=>{
                    that.updProd();
                    that.updCart();
                }
            }
            $(rm).find('#rm').on('click', event => {
                Controller.rmCartClicked( params );
            });
            $(rm).find('#dec').on('click', event => {
                Controller.decCartClicked( params );
            });
            
        })
        
        that.updProd();
    }
    
    this.addCartNavi = function(){
    }
}