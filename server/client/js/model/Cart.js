console.log('cart loaded');

function CartTable(cart){
    var table = cart || []; //if product selected, keep count
    var viewID = []; //keep viewID of this

    this.add = function(id, count) {
        // console.log(id+' '+count);
        if(table[id]){
            table[id] += +count;
        } else {
            table[id] = +count;
            updView();
        }
        // console.log(table);
    }
    this.remove = function(id){
        // id = viewID[id];
        // var count = table[id];
        // table[id] = null;
        // updView();
        // return count;
        return this.decrement(id, table[viewID[id]]);
    }
    this.decrement = function(id, count){
        id = viewID[id];
        if(table[id] < +count)
            return 0;
        table[id] -= +count;
        setTimeout(() => {
            updView();
        }, 0);
        return count;
    }
    function updView(){
        viewID = [];
        table.forEach((row, id) => {
            if(row)
                viewID.push(id);
        });
    }

    this.getView = function(){
        return viewID;
    }
    this.getCart = function(){
        return table;
    }
    this.getCount = function(view){
        return table[viewID[view]];
    }

    updView();
}