console.log('product loaded');

function ProductTable(){
    var table = [];

    this.remove = function (id, count){
        if(table[id].count < +count)
            return 1;
        table[id].count -= +count;
        return 0;
    }
    this.add = function (id, count){
        if(count){
            table[id].count += +count;
            return 0;
        }
        return 1;
    }

    this.parseList = function(){
        ProductTable.getList();

        ProductTable.list.forEach((prod, id) => {
            table.push({
                "name" : prod[0],
                "price" : prod[1],
                "count" : prod[2]
            })
        })
    }

    this.getTable = function (){
        var list = [];
        table.forEach((prod, id) => {
            list.push(this.getById(id));
        });
        return list;
    }
    this.getById = function(id){
        var prod = table[id];
        return [
            prod.name,
            prod.price,
            prod.count
        ]
    }
    this.getAttr = function(id, attr){
        return table[id][attr];
    }

    this.parseList();
}
ProductTable.list = [];

ProductTable.getList = function(){
    var xhr = new XMLHttpRequest();

    xhr.open(
        'GET', 
        dataUrl+'products.json', 
        // true
        false
    );
    
    // xhr.onreadystatechange = function() {
    //     if (this.readyState !== 4) {return;}
    //     if (this.status !== 200) {    
    //         alert( this.status + ': ' + this.statusText );
    //         return;
    //     }
    //     var res = JSON.parse(xhr.responseText);
    //     console.log('get: v')
    //     console.log(res);
    //     User.list = res;
    // };

    xhr.send();

    if (xhr.status !== 200) {
        alert( xhr.status + ': ' + xhr.statusText );
        return;
    }
    var res = JSON.parse(xhr.responseText);
    console.log('get: v')
    console.log(res);
    ProductTable.list = res || [];
}