console.log('shop loaded');

function Shop (){
    var user;
    var table = new ProductTable();
    var head = ['Name of product', 'Prc($)', 'cnt'];
    var cart;

    this.tryLogin = function(log, pass){
        var res = User.checkPass(log, pass)
        if(!res.code){
            // console.log(res);
            user = new User(res.user);
            cart = new CartTable(user.getCart());
            return 0;
        }
        return 1;
    }
    this.getUsername = function(){
        return user.name;
    }

    this.getTable = function(){
        return table.getTable();
    }
    this.getCartTable = function() {
        // console.log('--- getCartTable ---')
        var tab = [];
        var total = 0;
        cart.getView().forEach((id, cartid) => {
            if(id != null){
                var row = getCartRow(id, cartid);
                // console.log(row)
                tab.push(row);
                total += row[3];
            }
        });
        // console.log(tab);
        return {
            "table" : tab,
            "total" : total
        };

        function getCartRow(id, cartid) {
            var pr = table.getAttr(id, 'price');
            var c = cart.getCount(cartid);
            return [
                table.getAttr(id, 'name'),
                pr,
                c,
                pr*c
            ];
        }
    }
    this.addToCart = function(id, cnt) {
        if(!table.remove(id, cnt)){
            cart.add(id, cnt);
            return 0;
        }
        return 1;
    }
    this.removeFromCart = function(id) {
        return table.add(
            cart.getView()[id], 
            cart.remove(id)
        );
    }
    this.decrementInCart = function(id, cnt) {
        // console.log('dec')
        return table.add(
            cart.getView()[id], 
            cart.decrement(id, cnt)
        );
    }
                
    this.getProdHead = function() {
        return head.concat('Add item');
    }
    this.getCartHead = function() {
        return this.getCheckHead().concat('Remove');
    }
    this.getCheckHead = function(){
        return head.concat('Total');
    }
    
}
