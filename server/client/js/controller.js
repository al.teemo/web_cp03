function Controller(){}

Controller.checkLogin = function(success, failure, log, pass) {
    // console.log('result: '+log+' '+pass+' '+res);
    setTimeout(()=>{
        console.log('start check login')
        var res = tryLogin(log,pass);
        !res.code ? success() : failure();
        console.log('finish check login')
    }, 0);
}
Controller.crShop = function(render) {
    Controller.getRequest(render, getTableHeads);
}
Controller.getCheck = function(render){
    Controller.getRequest(render, getCheck);
}
Controller.getRequest = function(call, request){
    call( request() );
}
Controller.getUsername = function(update){
    Controller.callRequest(update, getUsername);
}
Controller.updTable = function(update){
    Controller.callRequest(update, getProdTable);
}
Controller.updCart = function(update){
    console.log('updCart')
    Controller.callRequest(update, getCartTable);
}
Controller.callRequest = function(call, request){
    setTimeout(() => {
        var res = request();
        if(!res.code){
            // console.log(res.data)
            call(res.data);
        } else {
            console.log('Error code: '+res.code);
            console.log(request);
            console.log(res.data);
        }
    }, 0);
}

Controller.addItem = function(id, count, update) {
    // setTimeout( addToCart, 0, id, count,
    //     Page.currPage.updCartTable
    // )
    setTimeout(() => {
        var res = addToCart(id, count);
        if(!res.code){
            // console.log(res.data)
            update(res.data);
        }
    }, 0);
}
Controller.rmCartClicked = function(params) {
    Controller.callDelete(params, removeFromCart);
}
Controller.decCartClicked = function(params) {
    Controller.callDelete(params, decrementInCart);
}
Controller.callDelete = function(params, callRemove) {
    setTimeout(() => {
        var res = callRemove(params.id);
        if(!res.code){
            params.remove();
        }
    }, 0);
}

// *** ON LOAD ***
$(document).ready(function () {
    Page.newPage(new LoginPage());
    Page.newPage(new ShopPage());
    Page.newPage(new CheckPage());

    Page.getStart();

//test
    var timeStart = 2000;
    var timeAdd = 1000;
    $('#login').val('l1');
    $('#password').val('p1');
    setTimeout(() => {
        $('input#logok').trigger('click');

        for(var i=0; i<3; i++)
            setTimeout((i) => {
                $('input#addt'+i).val(i*2+1);
                $('input#addb'+i).trigger('click');
            }, (i+1)*timeAdd, i);

        setTimeout(() => {
            $('#buy').trigger('click');
        }, 6*timeAdd + 1*timeStart);
    }, timeStart);

    
    
    console.log('---- loaded ----');
});

